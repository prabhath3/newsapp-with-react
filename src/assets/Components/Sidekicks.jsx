import React from "react";

function SideKicks(props){
    const article = props.article

    return(
        <article className="flex gap-5">
            <img src={article.image? article.image : "https://talentclick.com/wp-content/uploads/2021/08/placeholder-image.png"} alt="" className="rounded-lg w-1/4" />
            <div>
                <h2 className="text-lg font-bold mb-2">{article.title}</h2>
                <p>{article.description}</p>
            </div>
        </article>
    )
}

export default SideKicks