import React from "react";

function MainNews(props){
const article = props.article
    return(
        <article className="lg:row-start-1 row-span-3">
            <img src={article.image? article.image : "https://talentclick.com/wp-content/uploads/2021/08/placeholder-image.png"} alt="" className="rounded-lg w-full" />
            <div>
                <h2 className="text-4xl font-bold mt-5 mb-10">{article.title}</h2>
                <p>{article.description}</p>
            </div>
        </article>
    )
}

export default MainNews