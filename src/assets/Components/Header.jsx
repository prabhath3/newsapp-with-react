import React from "react";

function Header(props){

    return(
        <header className="border-b-2 border-purple-700">
  <div className="container px-5 py-2 mx-auto flex flex-row items-center">
    <a className="flex flex-row items-center" href="#">
      <img className="w-1/6" src="./images/icon.png" alt="" />
      <span className="font-bold text-3xl xl:text-4xl text-purple-900 hover:text-yellow-600">
        PRABHATH DAILY
      </span>
    </a>
    <div>
      <button className="md:hidden">
        <span className="material-symbols-outlined">menu</span>
      </button>
      <nav className="hidden md:flex ">
        <ul className="flex flex-row items-center gap-5 text-xl text-purple-900">
          <li className="hover:font-bold hover:text-purple-700">
            <a href="#">Home</a>
          </li>
          <li className="hover:font-bold hover:text-purple-700">
            <a href="#">Exclusive</a>
          </li>
          <li className="hover:font-bold hover:text-purple-700">
            <a href="#">Login</a>
          </li>
          <li className="hover:font-bold hover:text-purple-700">
            <a href="#">Contact</a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</header>
    )
}

export default Header