import { useEffect, useState } from 'react'
import Header from './assets/Components/Header'
import MainNews from './assets/Components/Mainnews'
import SideKicks from './assets/Components/Sidekicks'
import './App.css'
import axios from 'axios'

function App() {
  const [headlines, setHeadlines] = useState(null)
  //IF not Using Axio
/* fetch('API code')
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.log(error))*/
//to run first when loading page.
//axios is a library used to call API rather than fetch
  useEffect(()=>{
    const API_KEY= import.meta.env.VITE_API_KEY
    axios.get(`https://gnews.io/api/v4/top-headlines?category=general&max=4&country=in&apikey=${API_KEY}`)
    .then(data => {
      setHeadlines(data.data.articles)
    console.log(data.data)
    })
    
    .catch(error => console.log(error))
  },[])

  return (
    <>
      <Header />
      <>
  <main className="container mx-auto">
    <section className="section1 py-20">
      <h1 className="text-6xl mb-10">Happening NOW!!!</h1>

      {
        headlines?
        <div
        id="articlediv"
        className="grid gap-5 lg:grid-cols-2 lg:grid-rows-3 lg:gap-8 xl:gap-16">
          <MainNews article={headlines[0]} />
          <SideKicks article={headlines[1]} />
          <SideKicks article={headlines[2]} />
          <SideKicks article={headlines[3]} />
      </div>:<h2 className="text-4xl font-bold mt-5 mb-10">Loading....</h2>
      }
      
    </section>
  </main>
  <footer></footer>
</>

    </>
  )
}

export default App
